import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)      
import XMonad.Actions.NoBorders
import XMonad.Actions.WithAll (killAll)
import XMonad.Actions.WindowGo (runOrRaise)

import Data.Maybe (fromJust)
import Data.Maybe (isJust)
import Data.Monoid
import qualified Data.Map        as M

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docks, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.Hooks.SetWMName

import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8

import XMonad.Layout.Simplest
import XMonad.Layout.ResizableTile

import XMonad.Layout.LayoutModifier
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed      
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
  
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

import Colors.Dracula

myModMask :: KeyMask
myModMask = mod4Mask -- sets modkey to super/windows key

myTerminal :: String
myTerminal = "urxvtc" -- sets default terminal

myBrowser1 :: String
myBrowser1 = "brave"

myBrowser2 :: String
myBrowser2 = "qutebrowser"

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs'"

myFont :: String
myFont = "Zekton"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth = 2

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["www", "dev", "term", "games", "files", "chat", "media", "flex1", "flex2", "music"]
--myWorkspaces    = ["", "", "", "", "", "", "", "", "", "♫"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [0..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices   
-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
--
--    -- launch a terminal
--    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
--
--    -- launch dmenu
--    , ((modm,               xK_p     ), spawn "dmenu_run")
--
--    -- launch gmrun
--    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")
--
--    -- close focused window
--    , ((modm .|. shiftMask, xK_c     ), kill)
--
--     -- Rotate through the available layout algorithms
      [ ((modm,               xK_space ), sendMessage NextLayout)
--
--    --  Reset the layouts on the current workspace to default
--    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
--
--    -- Resize viewed windows to the correct size
--    , ((modm,               xK_n     ), refresh)
--
--    -- Move focus to the next window
--    , ((modm,               xK_Tab   ), windows W.focusDown)
--
--    -- Move focus to the next window
--    , ((modm,               xK_j     ), windows W.focusDown)
--
--    -- Move focus to the previous window
--    , ((modm,               xK_k     ), windows W.focusUp  )
--
--    -- Move focus to the master window
--    , ((modm,               xK_m     ), windows W.focusMaster  )
--
--    -- Swap the focused window and the master window
--    , ((modm,               xK_Return), windows W.swapMaster)
--
--    -- Swap the focused window with the next window
--    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
--
--    -- Swap the focused window with the previous window
--    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
--
--    -- Shrink the master area
--    , ((modm,               xK_h     ), sendMessage Shrink)
--
--    -- Expand the master area
--    , ((modm,               xK_l     ), sendMessage Expand)
--
--    -- Push window back into tiling
--    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
--
--    -- Increment the number of windows in the master area
--    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))
--
--    -- Deincrement the number of windows in the master area
--    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))
--
--    -- Toggle the status bar gap
--    -- Use this binding with avoidStruts from Hooks.ManageDocks.
--    -- See also the statusBar function from Hooks.DynamicLog.
--    --
--    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)
--
--    -- Quit xmonad
--    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
--
--    -- Restart xmonad
--    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")
--
--    -- Run xmessage with a summary of the default keybindings (useful for beginners)
--    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1,xK_2,xK_3,xK_4,xK_5,xK_6,xK_7,xK_8,xK_9,xK_0]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    -- mod-{y,u,i}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{y,u,i}, Move client to screen 1, 2, or 3
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_y, xK_u, xK_i] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

--    --
--    -- mod-[1..9], Switch to workspace N
--    -- mod-shift-[1..9], Move client to workspace N
--    --
--    [((m .|. modm, k), windows $ f i)
--        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
--        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
--    ++
--
--    --
--    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
--    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
--    --
--    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
--        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
--        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
--

-- Start_Keys
myKeysP :: [(String, X ())]
myKeysP =
    -- KB_GROUP XMonad keys
        [ ("M-C-r", spawn "xmonad --recompile")       -- Recompiles XMonad,
        , ("M-S-r", spawn "xmonad --restart")         -- Restarts xmonad
        , ("M-S-q", io exitSuccess)                   -- Quits xmonad
        , ("M-S-/", spawn "~/.local/bin/keybindings.sh")

    -- KB_GROUP Dmenu
        , ("M-d", spawn "dmenu_run")                  -- Launches dmenu

    -- KB_GROUP useful programs to bind
        , ("M-<F1>", spawn (myBrowser1))
        , ("M-<F2>", spawn (myEmacs))

        , ("M-<Return>", spawn (myTerminal))
        , ("M-b", spawn (myBrowser2))
        , ("M-x", spawn "arcolinux-logout")

    -- KB_GROUP Games
        , ("M-g f", spawn "$HOME/.steam/steam/steamapps/common/Factorio/bin/x64/factorio") -- Launches Factorio
        , ("M-g s", spawn "IronyModManager") --launches Irony Mod Manager (Stellaris)

     -- KB_GROUP Capslock
        , ("M-c s", spawn "setxkbmap -option caps:escape") -- Sets Caps Lock to Escape (s for set)
        , ("M-c u", spawn "setxkbmap -option")             -- Returns Capslock to Normal (u for unset)

        -- KB_GROUP Meta + shift
        , ("M-S-h", spawn (myTerminal ++ "cmus"))
        , ("M-S-s", spawn "xfce4-screenshooter")
        , ("M-S-<Return>", spawn "thunar")

    -- KB_GROUP close windows
        , ("M-q", kill1)        --kills current window
        , ("M-S-q", killAll)    --kills all windows on current workspace

    -- KB_GROUP Increase/decrease spacing (gaps)
        , ("C-M1-j", decWindowSpacing 1)         -- Decrease window spacing
        , ("C-M1-k", incWindowSpacing 1)         -- Increase window spacing
        , ("C-M1-h", decScreenSpacing 1)         -- Decrease screen spacing
        , ("C-M1-l", incScreenSpacing 1)         -- Increase screen spacing

    -- KB_GROUP Windows navigation
        , ("M-m", windows W.focusMaster)  -- Move focus to the master window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window

        , ("M-j", windows W.focusDown)    -- Move focus to the next window
        , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window

        , ("M-k", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
    -- KB_GROUP Window Resizing
        , ("M-h", sendMessage Shrink)               -- Shrink Horizontal Window Width
        , ("M-l", sendMessage Expand)               -- Expand Horizontal Window Width
        , ("M-M1-j", sendMessage MirrorShrink)      -- Shrink Vertical Window Width
        , ("M-M1-k", sendMessage MirrorExpand)      -- Expand Vertical Window Width

    -- KB_GROUP Workspace Navigation


    -- KB_GROUP Fullscreen
        , ("M-t b", withFocused toggleBorder) -- toggles no borders
        , ("M-t f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

    -- KB_GROUP Push Floating back to tile
        , ("M-S-t", withFocused $ windows . W.sink)  -- Push floating window back to tile

    -- KB_GROUP Scratchpads
        , ("M-t t", namedScratchpadAction myScratchPads "terminal")

    -- KB_GROUP Multimedia Keys
        , ("<XF86AudioPlay>", spawn "mocp --play")
        , ("<XF86AudioPrev>", spawn "mocp --previous")
        , ("<XF86AudioNext>", spawn "mocp --next")
        , ("<XF86AudioMute>", spawn "amixer set Master toggle")
        , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
        , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
        , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
        , ("<Print>", spawn "dm-maim")

    -- KB_GROUP Brightness
        , ("<XF86MonBrightnessUp>", spawn "brightnessctl s +1%")
        , ("S-<XF86MonBrightnessUp>", spawn "brightnessctl s +10%")
        , ("<XF86MonBrightnessDown>", spawn "brightnessctl s 1%-")
        , ("S-<XF86MonBrightnessDown>", spawn "brightnessctl s 10%-")

        ]
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

--End_Keys

-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

--This adds a configuarble amount of spacing
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True
-- This is the same except with no borders applied, so a single window will have no gaps              
              
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True
               
tall     = renamed [Replace "tall"]
          $ smartBorders
          $ windowNavigation
          $ mySpacing 5
          $ ResizableTall 1 (3/100) (1/2) []

myLayout = avoidStruts $ myDefaultLayout 
  where
    myDefaultLayout = withBorder myBorderWidth tall
      ||| noBorders Full
      ||| Mirror tall
     -- default tiling algorithm partitions the screen into two panes

     -- The default number of windows in the master pane

     -- Default proportion of screen occupied by master pane

     -- Percent of screen to increment by when resizing panes

myLogHook :: D.Client -> PP
myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    , ppCurrent = wrap ("%{F" ++ color06 ++ "} ") " %{F-}"
    , ppVisible = wrap ("%{F" ++ color06 ++ "} ") " %{F-}"
    , ppUrgent = wrap ("%{F" ++ color06 ++ "} ") " %{F-}"
    , ppHidden = wrap ("%{F" ++ color05 ++ "} ") " %{F-}"
    , ppTitle = wrap ("%{F" ++ color16 ++ "} ") " %{F-}"

    }

dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , className =? "arcolinux-logout" --> doFloat
    , className =? "Yad"            --> doCenterFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    ] <+> namedScratchpadManageHook myScratchPads

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                ]

    where
      spawnTerm = myTerminal ++ " -name scratchpad"
      findTerm = resource =? "scratchpad"
      manageTerm = customFloating $ W.RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 -h
                   l = 0.95 -w

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
        --spawn "killall trayer" --kill current trayer
        spawn "$HOME/.config/polybar/launch.sh"
        
        spawnOnce "variety"                     -- This lets me have a rotating background
        spawnOnce "picom"                       -- the compositor
        spawnOnce "urxvtd &"
        spawnOnce "/usr/bin/emacs --daemon"     -- emacs daemon for the emacsclient
        spawnOnce "volumeicon"
        spawnOnce "nm-applet"
        spawnOnce "steam -silent"

        --spawn ("sleep 2 && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut false --expand true --monitor 1 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 20")

        setWMName "LG3D"

main :: IO ()
main = do
    --    xmproc0 <- spawnPipe ("xmobar -x 0 $HOME/.xmonad/xmobar/xmobarrc")
    --    xmproc1 <- spawnPipe ("xmobar -x 1 $HOME/.xmonad/xmobar/xmobarrc")
    --    xmproc2 <- spawnPipe ("xmobar -x 2 $HOME/.xmonad/xmobar/xmobarrc")
 --       dbus <- D.connectSession
 --   -- Requests access to the DBus name
 --       D.requestName dbus (D.busName_ "org.xmonad.Log")
 --               [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
        
        xmonad $ docks $ ewmh def {
          --This just overides fields in the default config.
          -- anything not overwritten uses stuff from the default config over in /xmonad/XMonad/Config.hs

      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook <+> manageDocks,
        handleEventHook    = ewmhDesktopsEventHook <+> fullscreenEventHook,
        startupHook        = myStartupHook,
        keys               = myKeys,
        logHook            = ewmhDesktopsLogHook --dynamicLogWithPP (myLogHook dbus)   -- $ xmobarPP
       --       -- XMOBAR SETTINGS
       --       { ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
       --                       >> hPutStrLn xmproc1 x   -- xmobar on monitor 2
       --                       >> hPutStrLn xmproc2 x   -- xmobar on monitor 3
       --         -- Current workspace
       --       , ppCurrent = xmobarColor color06 "" . wrap
       --                     ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
       --         -- Visible but not current workspace
       --       , ppVisible = xmobarColor color06 "" . clickable
       --         -- Hidden workspace
       --       , ppHidden = xmobarColor color05 "" . wrap
       --                    ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>" . clickable
       --         -- Hidden workspaces (no windows)
       --       , ppHiddenNoWindows = xmobarColor color05 ""  . clickable
       --         -- Title of active window
       --       , ppTitle = xmobarColor color16 "" . shorten 60
       --         -- Separator character
       --       , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"
       --         -- Urgent workspace
       --       , ppUrgent = xmobarColor color02 "" . wrap "!" "!"
       --         -- Adding # of windows on current workspace to the bar
       --       , ppExtras  = [windowCount]
       --         -- order of things in xmobar
       --       , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
       --       }
         } `additionalKeysP` myKeysP
