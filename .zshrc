# export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH="/home/$USER/.emacs.d/bin:$PATH"
export PATH="/home/zack/Irony\ Mod\ manager:$PATH"
export PATH="/home/$USER/.cargo/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

export ZSH=/usr/share/oh-my-zsh/

 ZSH_THEME="gallifrey"

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# ZSH_THEME_RANDOM_IGNORED=(pygmalion tjkirch_mod)

# CASE_SENSITIVE="true"

HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# DISABLE_MAGIC_FUNCTIONS=true

# DISABLE_LS_COLORS="true"

# DISABLE_AUTO_TITLE="true"

ENABLE_CORRECTION="true"

COMPLETION_WAITING_DOTS="true"

# DISABLE_UNTRACKED_FILES_DIRTY="true"

HIST_STAMPS="mm/dd/yyyy"

setopt GLOB_DOTS

setopt SHARE_HISTORY

[[ $- != *i* ]] && return

export HISTCONTROL=ignoreboth:erasedups

export EDITOR='vim'
export VISUAL='vim'

plugins=(git zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#ZSH_CUSTOM=/"$HOME/$USER/.config/zsh/oh-my-zsh/"

alias update-fc='sudo fc-cache -fv'

alias cheat="cht.sh"

alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

alias grep='grep --color=auto'

alias rmgitcache="rm -r ~/.cache/git"

alias gitdf='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

alias cd..='cd ..'
alias pdw="pwd"

alias free="free -mt"

alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -alFh'

alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

alias hw="hwinfo --short"

alias capsEsc="xmodmap -e 'clear lock' -e 'keycode 0x42 = Escape'"

alias vim="nvim"

alias unlock="sudo rm /var/lib/pacman/db.lck"
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"

alias df='df -h'

alias rg="rg --sort path"

alias ssn="sudo shutdown now"
alias sr="sudo reboot"

alias integrated="sudo optimus-manager --switch integrated --no-confirm"
alias hybrid="sudo optimus-manager --switch hybrid --no-confirm"
alias nvidia="sudo optimus-manager --switch nvidia --no-confirm"

alias ls="exa"
alias cat="bat"

alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'

alias upall="yay -Syu --noconfirm"

alias wget="wget -c"

alias whichvga="/usr/local/bin/arcolinux-which-vga"

alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-best="yt-dlp --extract-audio --audio-format best "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias ytv-best="yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 "

eval $(thefuck --alias)

bindkey -v
export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

neofetch
