(beacon-mode 1)

(map! :leader
      (:prefix ("c h" . "Help info from Clippy")
       :desc "Clippy describes function under point" "f" #'clippy-describe-function
       :desc "Clippy describes variable under point" "v" #'clippy-describe-variable))

(setq company-idle-delay .3)

(map! :leader
      :desc "toggle Auto Complete"
      "t a" #'+company/toggle-auto-completion)

(setq dap-lldb-terminal "urxvt -e " )

(use-package! elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(add-hook 'elfeed-show-mode-hook 'visual-line-mode)
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(setq elfeed-feeds (quote
                    (("https://www.reddit.com/r/linux.rss" reddit linux)
                     ("https://www.reddit.com/r/commandline.rss" reddit commandline)
                     ("https://www.reddit.com/r/emacs.rss" reddit emacs)
                     ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
                     ("https://hackaday.com/blog/feed/" hackaday linux)
                     ("https://opensource.com/feed" opensource linux)
                     ("https://linux.softpedia.com/backend.xml" softpedia linux)
                     ("https://itsfoss.com/feed/" itsfoss linux)
                     ("https://www.zdnet.com/topic/linux/rss.xml" zdnet linux)
                     ("https://www.phoronix.com/rss.php" phoronix linux)
                     ("http://feeds.feedburner.com/d0od" omgubuntu linux)
                     ("https://www.computerworld.com/index.rss" computerworld linux)
                     ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)
                     ("https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux)
                     ("https://betanews.com/feed" betanews linux)
                     ("http://lxer.com/module/newswire/headlines.rss" lxer linux)
                     ("https://distrowatch.com/news/dwd.xml" distrowatch linux)
                     ("https://itsfoss.com/feed/" itsfoss linux))))

(map! :leader
      :desc "open Elfeed"
      "e o" #'elfeed)

(map! :leader
      :desc "update Elfeed"
      "e u" #'elfeed-update)

(setq doom-font (font-spec :family "Hack" :size 15 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "Liberation Serif" :size 15))

(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("org-plain-latex"
               "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setq display-line-numbers-type t)

;(setq user-mail-address "zbeckr@gmail.com"
;      user-full-name "Zack Becker"
;      mu4e-get-mail-command "mbsync -C ~/.config/mu4e/mbsyncrc -a"
;      mu4e-update-interval 300
;      messege-send-mail-function 'smtpmail-send-it
;      starttls-use-gnutls t
;      smtp-starttls-credentials'(("smtp.landl.com" 587 nil nil))
;      smtpmail-auth-credentials '(("smtp.landl.com" 587 "zbeckr@gmail.com" nil))
;      smtpmail-default-smtp-server "smtp.landl.com"
;      smtpmail-smtp-server "smtp.landl.com"
;      smtpmail-smtp-service 587
;      mu4e-sent-folder "/Sent"
;      mu4e-drafts-folder "/Drafts"
;      mu4e-trash-folder "/Trash"
;      mu4e-refile-folder "/All Mail")

(define-key evil-normal-state-map (kbd "g j") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "g k") 'evil-previous-visual-line)

(setq org-directory "~/org/")
(add-hook 'org-mode-hook 'visual-line-mode)

(setq org-hide-emphasis-markers t)

(setq org-roam-capture-templates
 '(("d" "default" plain ;; the "d" is the letter you press to select, the "default is the name"
   "%?"  ;;This is where your cursor goes
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n") ;;this line  will basically be in every capture template
   :unnarrowed t) ;;this line also will be in basically every capture template
  ("b" "book notes" plain
   "\n* Source\n\nAuthor: %^{Author}\nTitle: ${title}\nYear: %^{Year}\n\n* Summary\n\n%?"
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
   :unnarrowed t)
  ("n" "notes" plain
   (file "~/org/template/roam/BasicNoteTemplate.org")
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
    :unnarrowed t)
    )
 )

(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam ;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
    )
)

(map! :leader
      :desc "Open Org-roam UI Mode"
      "n r u" #'org-roam-ui-mode)

;;  (use-package pdf-tools
;;   :pin manual ;; manually update
;;   :config
;;   ;; initialise
;;   (pdf-tools-install)
;;   ;; open pdfs scaled to fit width
;;   (setq-default pdf-view-display-size 'fit-width)
;;   ;; use normal isearch
;;   (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
;;   :custom
;;   (pdf-annot-activate-created-annotations t "automatically

;(define-globalized-minor-mode global-rainbow-mode rainbow-mode
;(lambda () (rainbow-mode 1)))
;(global-rainbow-mode 1 )

(map! :leader
      :desc "Enable Selectric Mode"
      "t s" #'selectric-mode)

(setq doom-theme 'doom-dracula)

(map! :leader
      :desc "Switch to variable pitch mode"
      "t v" #'variable-pitch-mode)
