#!/usr/bin/env bash

 sed -n '/Start_Keys/,/End_Keys/p' ~/.xmonad/xmonad.hs | \
     grep -e ', ("' \
     -e '\[ (' \
     -e 'KB_GROUP' | \
     grep -v '\-\- , ("' | \
     sed -e 's/^[ \t]*//' \
         -e 's/, (/(/' \
         -e 's/\[ (/(/' \
         -e 's/-- KB_GROUP /\n/' \
         -e 's/", /"\t: /' | \
         yad --text-info --back=#282a36 --fore=##f8f8f2 --geometry=1200x800
